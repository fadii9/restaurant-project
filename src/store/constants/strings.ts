export const CUSTOMERS_STATE_TEXT = {
    STATE_NAME: "customers",
};

export const QUEUES_STATE_TEXT = {
    STATE_NAME: "queues",
};

export const SLOT_STATE_TEXT = {
    STATE_NAME: "slots",
};

export const DELIVERIES_STATE_TEXT = {
    STATE_NAME: "deliveries",
};