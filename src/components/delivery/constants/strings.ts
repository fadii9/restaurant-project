export const DELIVERIES_CARD_TEXT = {
    TITLE: "Deliveries Status",
    CARD_TITLE: "Delivery ",
    ORDER_ID_TITLE: "Order ID: ",
    DELIVERY_TIME_TITLE: "Delivery Time:",
    DELIVERING_STATUS: "Delivering",
    WAITING_STATUS: "Waiting...",
    NOT_IN_USE: "Not In Use",
};